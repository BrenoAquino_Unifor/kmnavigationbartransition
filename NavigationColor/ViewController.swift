//
//  ViewController.swift
//  NavigationColor
//
//  Created by Breno Pinheiro Aquino on 30/08/17.
//  Copyright © 2017 Breno Pinheiro Aquino. All rights reserved.
//

import UIKit
import SwifterSwift
import KMNavigationBarTransition

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let colorsNumber = ["#fff16364",
                        "#fff58559",
                        "#fff9a43e",
                        "#ffe4c62e",
                        "#ff67bf74",
                        "#ff59a2be",
                        "#ff2093cd",
                        "#ffad62a7",
                        "#ff805781"]
    
    var colors: [UIColor] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for color in self.colorsNumber {
            
            self.colors.append(UIColor(hexString: color)!)
        }
        
        self.navigationController?.navigationBar.barTintColor = UIColor.gray
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(color: UIColor.gray, size: CGSize(width: 1, height: 1)), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage(color: UIColor.gray, size: CGSize(width: 1, height: 1))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK: - Prepare for Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueDetalhes" {
            
            if let controller = segue.destination as? PageMenuDisciplinaViewController {
                
                if let indexPath = self.tableView.indexPathForSelectedRow {
                 
                    controller.color = self.colors[indexPath.row]
                }
            }
        }
    }
    
    // MARK: - TableView Methods
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 9
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.contentView.backgroundColor = self.colors[indexPath.row]
        
        cell.textLabel?.text = "\tView Controller"
        
        cell.textLabel?.textColor = colors[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "segueDetalhes", sender: nil)
        
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    

}

