//
//  DetalhesViewController.swift
//  NavigationColor
//
//  Created by Breno Pinheiro Aquino on 30/08/17.
//  Copyright © 2017 Breno Pinheiro Aquino. All rights reserved.
//

import UIKit

import PageMenu

class PageMenuDisciplinaViewController: UIViewController, CAPSPageMenuDelegate {

    var pageMenu: CAPSPageMenu?
    
    var color: UIColor!
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Sistemas Inteligentes"
        
        // MARK: Cor da NavigationBar
        self.navigationController?.navigationBar.barTintColor = color
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(color: color, size: CGSize(width: 1, height: 1)), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage(color: color, size: CGSize(width: 1, height: 1))
        
        // MARK: PageMenu
        self.pageMenu?.delegate = self
        
        var controllers: [UIViewController] = []
        
        let controllerFrequencia = FrequenciaViewController(nibName: "FrequenciaViewController", bundle: nil)
        
        controllerFrequencia.title = "Frequencia"
        
        controllers.append(controllerFrequencia)
        
        let controllerAulas = AulasViewController(nibName: "AulasViewController", bundle: nil)
        
        controllerAulas.title = "Aulas"
        
        controllers.append(controllerAulas)
        
        let controllerDisciplina = DisciplinaViewController(nibName: "DisciplinaViewController", bundle: nil)
        
        controllerDisciplina.title = "Disciplina"
        
        controllers.append(controllerDisciplina)
        
        let transparente = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0)
        
        let parameters: [CAPSPageMenuOption] = [
            
            .menuItemSeparatorWidth(4.3),
            
            .useMenuLikeSegmentedControl(true),
            
            .menuItemSeparatorColor(transparente),
            
            .scrollMenuBackgroundColor(color),
            
            .menuItemSeparatorPercentageHeight(0.1),
            
            .unselectedMenuItemLabelColor(.white),
            
            .selectedMenuItemLabelColor(.white)
        ]
        
        self.pageMenu = CAPSPageMenu(viewControllers: controllers, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        
        self.view.addSubview(pageMenu!.view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
}
